#!/bin/bash

set -e

#
	
# make zip file and template README for this project
	
# 



#Edge.Cuts,User.Comments,User.Drawings,F.Mask,B.Mask,F.Silkscreen,B.Silkscreen,F.Paste,B.Paste,F.Cu,In1.Cu,In2.Cu,In3.Cu,In4.Cu,In5.Cu,In6.Cu,In7.Cu,In8.Cu,In9.Cu,In10.Cu,In11.Cu,In12.Cu,In13.Cu,In14.Cu,B.Cu

#PTH.drl,PTH-drl_map,NPTH.drl,NPTH-drl_map




date="2023-12-11"
base="APOLLO_SM_Rev3"
	
orig=carrier

mkdir -p fab
rm -f fab/*

#Build the Gerbers
kicad-cli pcb export gerbers --no-protel-ext --no-x2 --no-netlist --use-drill-file-origin -l Edge.Cuts,User.Comments,User.Drawings,F.Mask,B.Mask,F.Silkscreen,B.Silkscreen,F.Paste,B.Paste,F.Cu,In1.Cu,In2.Cu,In3.Cu,In4.Cu,In5.Cu,In6.Cu,In7.Cu,In8.Cu,In9.Cu,In10.Cu,In11.Cu,In12.Cu,In13.Cu,In14.Cu,B.Cu ../carrier.kicad_pcb


#Build the drill files
kicad-cli pcb export drill --format excellon --drill-origin plot --excellon-zeros-format decimal -u in --excellon-separate-th --generate-map --map-format gerberx2 ../carrier.kicad_pcb 

#pos files
kicad-cli pcb export pos --side front --format ascii --units mm --use-drill-file-origin ../carrier.kicad_pcb -o ${orig}-top.pos
kicad-cli pcb export pos --side back --format ascii --units mm --use-drill-file-origin ../carrier.kicad_pcb -o ${orig}-bottom.pos

#generate schematic
mkdir -p ../pdf
kicad-cli sch export pdf --output ../pdf/${orig}.pdf ../${orig}.kicad_sch

#generate BOM
kicad-cli sch export python-bom --output ../BOM/BOM.xml ../${orig}.kicad_sch
python3 "plugins/bom_csv_grouped_by_value.py" "../BOM/BOM.xml" "../BOM/BOM.csv"
kibom ../BOM/BOM.xml ../BOM/${base}.csv


#board info
mv ${orig}-Edge_Cuts.gbr      fab/${base}-EdgeCuts.pho
mv ${orig}-User_Comments.gbr  fab/${base}-Cmts_User.pho
mv ${orig}-User_Drawings.gbr  fab/${base}-Fab_dwg.pho

#masks
mv ${orig}-F_Mask.gbr         fab/${base}-F_Mask.pho	
mv ${orig}-B_Mask.gbr         fab/${base}-B_Mask.pho	

#silkscreen
mv ${orig}-F_Silkscreen.gbr   fab/${base}-F_SilkS.pho	
mv ${orig}-B_Silkscreen.gbr   fab/${base}-B_SilkS.pho

#paste
mv ${orig}-F_Paste.gbr        fab/${base}-F_Paste.pho
mv ${orig}-B_Paste.gbr        fab/${base}-B_Paste.pho

#copper
mv ${orig}-F_Cu.gbr           fab/${base}-Layer1.pho
mv ${orig}-In1_Cu.gbr         fab/${base}-Layer2.pho
mv ${orig}-In2_Cu.gbr         fab/${base}-Layer3.pho
mv ${orig}-In3_Cu.gbr         fab/${base}-Layer4.pho
mv ${orig}-In4_Cu.gbr         fab/${base}-Layer5.pho
mv ${orig}-In5_Cu.gbr         fab/${base}-Layer6.pho
mv ${orig}-In6_Cu.gbr         fab/${base}-Layer7.pho
mv ${orig}-In7_Cu.gbr         fab/${base}-Layer8.pho
mv ${orig}-In8_Cu.gbr         fab/${base}-Layer9.pho
mv ${orig}-In9_Cu.gbr         fab/${base}-Layer10.pho
mv ${orig}-In10_Cu.gbr        fab/${base}-Layer11.pho
mv ${orig}-In11_Cu.gbr        fab/${base}-Layer12.pho
mv ${orig}-In12_Cu.gbr        fab/${base}-Layer13.pho
mv ${orig}-In13_Cu.gbr        fab/${base}-Layer14.pho
mv ${orig}-In14_Cu.gbr        fab/${base}-Layer15.pho
mv ${orig}-B_Cu.gbr           fab/${base}-Layer16.pho

#drill
mv ${orig}-PTH.drl            fab/${base}-PTH.drl	
mv ${orig}-PTH-drl_map.gbr    fab/${base}-PTH-drl_map.pho	
mv ${orig}-NPTH.drl           fab/${base}-NPTH.drl
mv ${orig}-NPTH-drl_map.gbr   fab/${base}-NPTH-drl_map.pho

#assembly pos files
mv ${orig}-top.pos            fab/${base}-top.pos
mv ${orig}-bottom.pos         fab/${base}-bottom.pos

#dsn file
#mv ${orig}.dsn                fab/${base}.dsn

#cad file
#mv ${orig}.cad                fab/${base}.cad

#sch
cp ../pdf/${orig}.pdf          fab/${base}.pdf

#BOM
cp ../BOM/${base}.csv          fab/${base}-BOM.csv


#zip files
zip ${base}-${date}.zip fab/*



